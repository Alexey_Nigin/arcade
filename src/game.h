#ifndef GAME_H
#define GAME_H

#include "screen.h"


class Game {
public:

	// M: this
	// E: Creates a game attached to given screen and keyboard
	Game(Screen &screen_in);

	// R: difficulty is valid
	// M: this, screen, keyboard
	// E: Plays the game at specified difficulty, returns the score
	virtual int play(size_t difficulty) = 0;

	// M: this
	// E: Destroys this
	virtual ~Game();

protected:

	Screen &screen;

private:

	Game() = delete;

};

#endif