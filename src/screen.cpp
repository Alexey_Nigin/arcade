#include "screen.h"
#include "quit.h"
#include <iostream>
#include <ncurses.h>
#include <cassert>

using namespace std;





Screen::Too_small::Too_small(size_t rows_in, size_t cols_in)
: rows{rows_in}, cols{cols_in} {}

Screen::Screen()
: data{MAX_ROW + 2} {
	// Initialise ncurses
	initscr();

	// Check screen size
	getmaxyx(stdscr, console_rows, console_cols);
	if (console_rows < MAX_ROW + 2 || console_cols < MAX_COL + 2) {
		// Why is endwin() necessary here?
		// When an exception is thrown from the constructor, construction stops,
		// and since this Screen was never fully constructed, the compiler
		// doesn't bother destructing it, so we have to manually call endwin()
		// from the destructor before throwing. We can safely throw Too_small
		// from any other function in Screen, since by that time this Screen
		// will be fully constructed and the compiler will properly destruct it.
		endwin();
		throw Too_small(console_rows, console_cols);
	}

	// Set ncurses settings
	noecho();
	keypad(stdscr, true);
	curs_set(0);

	// Resize data
    for (auto &s : data) {
        s.resize(MAX_COL + 2, ' ');
    }

    // Draw border
    for (size_t i = 1; i < MAX_COL + 1; ++i) {
        data[0][i] = '-';
        data[MAX_ROW + 1][i] = '-';
    }
    for (size_t j = 1; j < MAX_ROW + 1; ++j) {
        data[j][0] = '|';
        data[j][MAX_COL + 1] = '|';
    }
    data[0][0] = '+';
    data[0][MAX_COL + 1] = '+';
    data[MAX_ROW + 1][0] = '+';
    data[MAX_ROW + 1][MAX_COL + 1] = '+';
}



char & Screen::operator() (size_t row, size_t col) {
    assert(row < MAX_ROW && col < MAX_COL);
    return data[row + 1][col + 1];
}



const char & Screen::operator() (size_t row, size_t col) const {
    assert(row < MAX_ROW && col < MAX_COL);
    return data[row + 1][col + 1];
}



void Screen::clear() {
    for (size_t i = 1; i < MAX_ROW + 1; ++i) {
        for (size_t j = 1; j < MAX_COL + 1; ++j) {
            data[i][j] = ' ';
        }
    }
}



void Screen::print(size_t row, size_t col, const std::string &to_print) {
    assert(row < MAX_ROW && col < MAX_COL);
    assert(to_print.length() + col < MAX_COL + 1);
    for (size_t i = 0; i < to_print.length(); ++i) {
        data[row + 1][col + i + 1] = to_print[i];
    } 
}



void Screen::update() const {
	// Check terminal size
	size_t old_rows = console_rows;
	size_t old_cols = console_cols;
	getmaxyx(stdscr, console_rows, console_cols);
	if (console_rows != old_rows || console_cols != old_cols) {
		if (console_rows < MAX_ROW + 2 || console_cols < MAX_COL + 2) {
			throw Too_small(console_rows, console_cols);
		}
		erase();
	}

	// Draw the screen
	size_t corner_row = (console_rows - MAX_ROW - 2) / 2;
	size_t corner_col = (console_cols - MAX_COL - 2) / 2;
	for (size_t row = 0; row < MAX_ROW + 2; ++row) {
		mvaddstr(static_cast<int>(corner_row + row),
						static_cast<int>(corner_col), data[row].c_str());
	}
	refresh();
}



bool Screen::get_key(Screen::Key &key, bool wait) const {
	nodelay(stdscr, !wait);

	switch (getch()) {
			case KEY_UP:
			case 'w':
				key = Screen::Key::UP;
				break;
			case KEY_LEFT:
			case 'a':
				key = Screen::Key::LEFT;
				break;
			case KEY_DOWN:
			case 's':
				key = Screen::Key::DOWN;
				break;
			case KEY_RIGHT:
			case 'd':
				key = Screen::Key::RIGHT;
				break;
			case '\n':
			case ' ':
				key = Screen::Key::SPACE;
				break;
			case 'p':
				pause();
				break;
			case 'q':
				quit();
				break;
			case ERR:
				return false;
			default:
				key = Screen::Key::OTHER;
	}

	return true;
}



Screen::~Screen() {
	endwin();
}



void Screen::pause() const {
	nodelay(stdscr, false);

	while (true) {
		switch(getch()) {
			case 'p':
				return;
			case 'q':
				quit();
		}
	}
}