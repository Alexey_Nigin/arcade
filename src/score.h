#ifndef SCORE_H
#define SCORE_H

#include <string>
#include <vector>

class Scores {
public:

    static const int NO_SCORE;

    // M: this
    // E: Creates empty scoreboard
    Scores();

    // M: this
    // E: Loads scoreboard from file
    void load();

    // M: filesystem
    // E: Saves scoreboard to file
    void save() const;

    // R: game and difficulty indices are valid
    // E: Gets scores
    std::vector<int> get(size_t game, size_t difficulty) const;

    // R: game and difficulty indices are valid
    // M: this
    // E: Writes new score to scoreboard
    void push(size_t game, size_t difficulty, int score);

private:

    const std::string FILENAME = ".scores";
    static const size_t NUM_SCORES = 10;
    std::vector<std::vector<int>> score_list;
};

#endif