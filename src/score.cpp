#include "score.h"
#include "lists.h"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <limits>

using namespace std;



const int Scores::NO_SCORE = numeric_limits<int>::min();

Scores::Scores() {
    vector<int> game_diff_def;
    game_diff_def.resize(NUM_SCORES, NO_SCORE);
    score_list.resize(GAME_LIST.size() * DIFFICULTY_LIST.size(), game_diff_def);
}

void Scores::load() {
    ifstream fin;
    fin.open(FILENAME);
    if (!fin.is_open()) return;
    for (size_t i = 0; i < score_list.size(); ++i) {
        for (size_t j = 0; j < NUM_SCORES; ++j) {
            fin >> score_list[i][j]; 
        }
    }
}

void Scores::save() const {
    ofstream fout;
    fout.open(FILENAME);
    for (size_t i = 0; i < score_list.size(); ++i) {
        for (size_t j = 0; j < NUM_SCORES; ++j) {
            fout << score_list[i][j] << ' '; 
        }
    }
}

std::vector<int> Scores::get(size_t game, size_t difficulty) const {
    size_t i = game * DIFFICULTY_LIST.size() + difficulty;
    return score_list[i];
}

void Scores::push(size_t game, size_t difficulty, int score) {
    size_t i = game * DIFFICULTY_LIST.size() + difficulty;
    score_list[i].push_back(score);
    sort(score_list[i].begin(), score_list[i].end(), greater<int>{});
    score_list[i].pop_back();
}