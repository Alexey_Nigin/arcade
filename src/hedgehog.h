#ifndef HEDGEHOG_H
#define HEDGEHOG_H

#include "speed_game.h"
#include "point.h"
#include "randomizer.h"



class Hedgehog : public Speed_game {
public:

	// M: this
	// E: Creates a Hedgehog attached to given screen and keyboard
	Hedgehog(Screen &screen_in);

	// M: this
	// E: Destroys this
	virtual ~Hedgehog();

private:

	// Types

	struct Cell {
		enum class Type {EMPTY, UNCLAIMED, CLAIMED, SOLID};

		Type type;
		size_t pheromones;
		bool ever_collected;
	};

	enum class Direction {UP, LEFT, DOWN, RIGHT, NONE};

	// Variables

	static const std::string DATAFILE;
	static const size_t ENEMY_SPAWN_PERIOD;
	static const char EMPTY_CHAR;
	static const char UNCLAIMED_CHAR;
	static const char CLAIMED_CHAR;
	static const char SOLID_CHAR;
	static const char HERO_CHAR;
	static const char ENEMY_CHAR;

	Randomizer ran;
	std::vector<std::vector<Cell>> map;
	std::vector<Point<size_t>> enemies;
	Point<size_t> hero, spawner, display;
	size_t time;
	Direction desired_dir;

	// Functions

	// M: this
	// E: Prepares the game by initialising all relevant member variables
	virtual void prepare() override;

	// R: difficulty is valid
	// E: Returns the interval between screen updates for given difficulty
	virtual Interval get_interval(size_t difficulty) override;

	// M: this
	// E: Handles a keypress by changing appropriate member variables
	virtual void handle_keypress(Screen::Key key) override;

	// M: this
	// E: Calculates a single tick of the game and returns true iff game over
	virtual bool tick() override;

	// M: this->{map, hero, spawner, display}, program flow (assert)
	// E: Loads the map from file, asserts that it is valid
	void load_map();

	// M: screen
	// Draws everything on the screen
	void draw_stuff();

	// M: this->{hero, desired_dir}
	// E: Moves the hedgehog
	void move_hero();

	// M: this->map
	// E: Paints the blocks on all sides of the hero
	void paint();

	// M: this->{map, score}
	// E: Collects complete islands around the player, making the blocks
	//    unclaimed and adding to the score
	void check_and_collect();

	// R: block is orthogonally adjacent to the hero
	// E: Returns true iff the island containing block is complete
	bool check_block(Point<size_t> block) const;

	// R: block is CLAIMED
	// M: this->{map, score}
	// E: Collects island containing block
	void collect_island(Point<size_t> block);

	// R: block is within map boundary
	// E: Returns true iff block is UNCLAIMED or CLAIMED
	bool is_island(Point<size_t> block) const;

	// M: this->{enemies, score}
	// E: Creates a new enemy at the spawner position, adds a bit to score
	void spawn_enemy();

	// M: this->{map, enemies}
	// E: Moves all enemies and updates pheromone concentrations
	void move_enemies();

	// E: Returns true iff the hero is colliding with one of the enemies
	bool collision() const;

};

#endif