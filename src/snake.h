#ifndef SNAKE_H
#define SNAKE_H

#include "speed_game.h"
#include "point.h"
#include "randomizer.h"
#include <queue>



class Snake : public Speed_game {
public:

	// M: this
	// E: Creates a snake attached to given screen and keyboard
	Snake(Screen &screen_in);

	// M: this
	// E: Destroys this
	virtual ~Snake();

private:

	// Types

	enum class Direction {UP, LEFT, DOWN, RIGHT};

	// Variables

	static const size_t GAME_ROWS;
	static const size_t GAME_COLS;
	static const size_t START_LENGTH;
	static const size_t START_COL;
	static const std::string SHRINKER_CHARS;
	static const size_t SHRINKER_SIZE;
	static const size_t SHRINKER_RARITY;
	static const size_t SHRINKER_POWER;
	static const char SKIN;
	static const char FOOD;

	std::queue<Point<size_t>> body;
	Randomizer ran;
	Point<size_t> head, shrinker_pos;
	size_t shrinker_age;
	Direction dir, dir_old;


	// Functions

	// M: this
	// E: Prepares the game by initialising all relevant member variables
	virtual void prepare() override;

	// R: difficulty is valid
	// E: Returns the interval between screen updates for given difficulty
	virtual Interval get_interval(size_t difficulty) override;

	// M: this
	// E: Handles a keypress by changing appropriate member variables
	virtual void handle_keypress(Screen::Key key) override;

	// M: this
	// E: Calculates a single tick of the game and returns true iff game over
	virtual bool tick() override;

	// R: Screen has empty space
	// M: screen
	// E: Places food in an empty free position on screen
	void place_food();

	// M: this->shrinker_pos, this->shrinker_age, screen
	// E: Try to place a shrinker in a random place on the screen; if there's
	//    not enough space, do nothing. 
	void place_shrinker();

	// R: shrinker_age <= SHRINKER_CHARS.size()
	// M: this->shrinker_age
	// E: If the shrinker isn't dead, age it a bit.
	void age_shrinker();

	// M: this->shrinker_age, screen
	// E: Draw the shrinker on screen; if the shrinker is dead, erase it while
	//    being careful not to erase the snake or its food.
	void draw_shrinker();

	// R: Bar edge and labels are drawn on screen
	// M: screen
	// E: Prints numbers to the info bar
	void update_bar();

};

#endif