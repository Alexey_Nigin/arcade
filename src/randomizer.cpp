#include "randomizer.h"

using namespace std;





Randomizer::Randomizer()
: engine{random_device{}()} {}



size_t Randomizer::get_size_t(size_t low, size_t high) {
	return uniform_int_distribution<size_t>{low, high - 1}(engine);
}