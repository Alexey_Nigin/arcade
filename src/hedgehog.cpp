#include "hedgehog.h"
#include <stack>
#include <fstream>
#include <cmath>
#include <cassert>

using namespace std;





Hedgehog::Hedgehog(Screen &screen_in)
: Speed_game{screen_in} {}



Hedgehog::~Hedgehog() {}



const string Hedgehog::DATAFILE = "hedgehog.dat";
const size_t Hedgehog::ENEMY_SPAWN_PERIOD = 75;
const char Hedgehog::EMPTY_CHAR = ' ';
const char Hedgehog::UNCLAIMED_CHAR = ':';
const char Hedgehog::CLAIMED_CHAR = 'I';
const char Hedgehog::SOLID_CHAR = 'H';
const char Hedgehog::HERO_CHAR = '@';
const char Hedgehog::ENEMY_CHAR = '#';



void Hedgehog::prepare() {
	load_map();
	desired_dir = Direction::NONE;
	enemies.clear();
	score = 0;
	time = 0;
}



Hedgehog::Interval Hedgehog::get_interval(size_t difficulty) {
	double framerate = pow(1.25, static_cast<double>(difficulty)) * 2;
	return Interval(1000 / framerate);
}



void Hedgehog::handle_keypress(Screen::Key key) {
	switch (key) {
		case Screen::Key::UP:
			desired_dir = Direction::UP;
			break;
		case Screen::Key::LEFT:
			desired_dir = Direction::LEFT;
			break;
		case Screen::Key::DOWN:
			desired_dir = Direction::DOWN;
			break;
		case Screen::Key::RIGHT:
			desired_dir = Direction::RIGHT;
			break;
		case Screen::Key::SPACE:
		case Screen::Key::OTHER:
			{}
	}
}



bool Hedgehog::tick() {
	if (!(time % ENEMY_SPAWN_PERIOD)) spawn_enemy();
	move_enemies();

	if (collision()) {
		draw_stuff();
		return true;
	}
	move_hero();
	if (collision()) {
		draw_stuff();
		return true;
	}

	paint();
	check_and_collect();

	draw_stuff();
	++time;
	return false;
}



void Hedgehog::load_map() {
	ifstream data(DATAFILE);
	// First line is the border
	data.ignore(numeric_limits<streamsize>::max(), '\n');

	// Iterate through the file
	#ifdef DEBUG
		bool found_hero = false;
		bool found_spawner = false;
	#endif
	bool found_display = false;
	map.resize(Screen::MAX_ROW);
	for (size_t row = 0; row < Screen::MAX_ROW; ++row) {
		map[row].resize(Screen::MAX_COL);

		// Read a line of data and check that it's okay
		string line;
		getline(data, line);
		assert(data);
		assert(line.size() >= Screen::MAX_COL + 2);

		// Decode the line - remember that the first char is the wall
		line.erase(0, 1);
		for (size_t col = 0; col < Screen::MAX_COL; ++col) {
			switch (line[col]) {
				case EMPTY_CHAR:
					map[row][col].type = Cell::Type::EMPTY;
					break;
				case UNCLAIMED_CHAR:
					map[row][col].type = Cell::Type::UNCLAIMED;
					break;
				case SOLID_CHAR:
					map[row][col].type = Cell::Type::SOLID;
					break;
				case HERO_CHAR:
					assert(!found_hero);
					hero.set(row, col);
					#ifdef DEBUG
						found_hero = true;
					#endif
					map[row][col].type = Cell::Type::EMPTY;
					break;
				case ENEMY_CHAR:
					assert(!found_spawner);
					spawner.set(row, col);
					#ifdef DEBUG
						found_spawner = true;
					#endif
					map[row][col].type = Cell::Type::EMPTY;
					break;
				case '*':
					if (!found_display) {
						display.set(row, col);
						found_display = true;
					}
					map[row][col].type = Cell::Type::SOLID;
					break;
				default:
					assert(false);
			}
			map[row][col].pheromones = 0;
			map[row][col].ever_collected = false;
		}
	}

	// By this point, we should've found everything
	assert(found_hero && found_spawner && found_display);

	// I don't want enemies sitting on the spawner
	map[spawner.row][spawner.col].pheromones = 1000000;

	// Note: I allow extra characters to the right and/or below the map area
}



void Hedgehog::draw_stuff() {
	for (size_t row = 0; row < Screen::MAX_ROW; ++row) {
		for (size_t col = 0; col < Screen::MAX_COL; ++col) {
			switch (map[row][col].type) {
				case Cell::Type::EMPTY:
					screen(row, col) = EMPTY_CHAR;
					break;
				case Cell::Type::UNCLAIMED:
					screen(row, col) = UNCLAIMED_CHAR;
					break;
				case Cell::Type::CLAIMED:
					screen(row, col) = CLAIMED_CHAR;
					break;
				case Cell::Type::SOLID:
					screen(row, col) = SOLID_CHAR;
					break;
			}
		}
	}

	screen.print(display.row, display.col, " score   ");
	screen.print(display.row+1, display.col, "         ");
	string score_line = to_string(score*multiplier) + ' ';
	while (score_line.size() < 9) {
		score_line = ' ' + score_line;
	}
	screen.print(display.row+2, display.col, score_line);

	screen(hero.row, hero.col) = HERO_CHAR;

	for (auto enemy : enemies) {
		screen(enemy.row, enemy.col) = ENEMY_CHAR;
	}
}



void Hedgehog::move_hero() {
	switch (desired_dir) {
		case Direction::UP:
			if (hero.row && map[hero.row-1][hero.col].type
							== Cell::Type::EMPTY) {
				--hero.row;
			}
			break;
		case Direction::LEFT:
			if (hero.col && map[hero.row][hero.col-1].type
							== Cell::Type::EMPTY) {
				--hero.col;
			}
			break;
		case Direction::DOWN:
			if (hero.row < Screen::MAX_ROW-1 && map[hero.row+1][hero.col].type
							== Cell::Type::EMPTY) {
				++hero.row;
			}
			break;
		case Direction::RIGHT:
			if (hero.col < Screen::MAX_COL-1 && map[hero.row][hero.col+1].type
							== Cell::Type::EMPTY) {
				++hero.col;
			}
			break;
		case Direction::NONE:
			{}
	}

	desired_dir = Direction::NONE;
}



void Hedgehog::paint() {
	if (hero.row && map[hero.row-1][hero.col].type == Cell::Type::UNCLAIMED) {
		map[hero.row-1][hero.col].type = Cell::Type::CLAIMED;
	}
	if (hero.col && map[hero.row][hero.col-1].type == Cell::Type::UNCLAIMED) {
		map[hero.row][hero.col-1].type = Cell::Type::CLAIMED;
	}
	if (hero.row < Screen::MAX_ROW-1 && map[hero.row+1][hero.col].type
					== Cell::Type::UNCLAIMED) {
		map[hero.row+1][hero.col].type = Cell::Type::CLAIMED;
	}
	if (hero.col < Screen::MAX_COL-1 && map[hero.row][hero.col+1].type
					== Cell::Type::UNCLAIMED) {
		map[hero.row][hero.col+1].type = Cell::Type::CLAIMED;
	}
}



void Hedgehog::check_and_collect() {
	if (hero.row && check_block({hero.row-1, hero.col})) {
		collect_island({hero.row-1, hero.col});
	}
	if (hero.col && check_block({hero.row, hero.col-1})) {
		collect_island({hero.row, hero.col-1});
	}
	if (hero.row < Screen::MAX_ROW-1 && check_block({hero.row+1, hero.col})) {
		collect_island({hero.row+1, hero.col});
	}
	if (hero.col < Screen::MAX_COL-1 && check_block({hero.row, hero.col+1})) {
		collect_island({hero.row, hero.col+1});
	}
}



bool Hedgehog::check_block(Point<size_t> block) const {
	// If the block isn't CLAIMED, it is either EMPTY/SOLID, in which case it's
	// not part of an island and there's nothing to do, or it is UNCLAIMED, in
	// which case it is next to the empty block where the hero sits and the
	// island is incomplete
	if (map[block.row][block.col].type != Cell::Type::CLAIMED) return false;

	// Create visited - a 2D vector that shows places I've been to
	vector<vector<bool>> visited{Screen::MAX_ROW};
	for (vector<bool>& c : visited) c.resize(Screen::MAX_COL, false);

	// Create island - a stack for DFS
	// INVARIANT: island can only contain UNCLAIMED and CLAIMED blocks; every
	//            block in the island must be visited
	stack<Point<size_t>> island;
	island.push(block);
	visited[block.row][block.col] = true;

	while (!island.empty()) {
		Point<size_t> b = island.top();
		island.pop();
		bool b_unclaimed = (map[b.row][b.col].type == Cell::Type::UNCLAIMED);
		// Look up
		if (b.row && !visited[b.row-1][b.col]) {
			if (b_unclaimed && map[b.row-1][b.col].type == Cell::Type::EMPTY) {
				return false;
			}
			if (is_island({b.row-1, b.col})) {
				island.emplace(b.row-1, b.col);
				visited[b.row-1][b.col] = true;
			}
		}
		// Look left
		if (b.col && !visited[b.row][b.col-1]) {
			if (b_unclaimed && map[b.row][b.col-1].type == Cell::Type::EMPTY) {
				return false;
			}
			if (is_island({b.row, b.col-1})) {
				island.emplace(b.row, b.col-1);
				visited[b.row][b.col-1] = true;
			}
		}
		// Look down
		if (b.row < Screen::MAX_ROW-1 && !visited[b.row+1][b.col]) {
			if (b_unclaimed && map[b.row+1][b.col].type == Cell::Type::EMPTY) {
				return false;
			}
			if (is_island({b.row+1, b.col})) {
				island.emplace(b.row+1, b.col);
				visited[b.row+1][b.col] = true;
			}
		}
		// Look right
		if (b.col < Screen::MAX_COL-1 && !visited[b.row][b.col+1]) {
			if (b_unclaimed && map[b.row][b.col+1].type == Cell::Type::EMPTY) {
				return false;
			}
			if (is_island({b.row, b.col+1})) {
				island.emplace(b.row, b.col+1);
				visited[b.row][b.col+1] = true;
			}
		}
	}

	return true;
}



void Hedgehog::collect_island(Point<size_t> block) {
	// Check REQUIRES
	assert(map[block.row][block.col].type == Cell::Type::CLAIMED);

	// Create visited - a 2D vector that shows places I've been to
	vector<vector<bool>> visited{Screen::MAX_ROW};
	for (vector<bool>& c : visited) c.resize(Screen::MAX_COL, false);

	// Create island - a stack for DFS
	// INVARIANT: island can only contain UNCLAIMED and CLAIMED blocks; every
	//            block in the island must be visited
	stack<Point<size_t>> island;
	island.push(block);
	visited[block.row][block.col] = true;

	while (!island.empty()) {
		Point<size_t> b = island.top();
		island.pop();
		score += map[b.row][b.col].ever_collected ? 1 : 3;
		map[b.row][b.col].type = Cell::Type::UNCLAIMED;
		map[b.row][b.col].ever_collected = true;
		// Look up
		if (b.row && !visited[b.row-1][b.col] && is_island({b.row-1, b.col})) {
			island.emplace(b.row-1, b.col);
			visited[b.row-1][b.col] = true;
		}
		// Look left
		if (b.col && !visited[b.row][b.col-1] && is_island({b.row, b.col-1})) {
			island.emplace(b.row, b.col-1);
			visited[b.row][b.col-1] = true;
		}
		// Look down
		if (b.row < Screen::MAX_ROW-1 && !visited[b.row+1][b.col]
						&& is_island({b.row+1, b.col})) {
			island.emplace(b.row+1, b.col);
			visited[b.row+1][b.col] = true;
		}
		// Look right
		if (b.col < Screen::MAX_COL-1 && !visited[b.row][b.col+1]
						&& is_island({b.row, b.col+1})) {
			island.emplace(b.row, b.col+1);
			visited[b.row][b.col+1] = true;
		}
	}
}



bool Hedgehog::is_island(Point<size_t> block) const {
	// Check REQUIRES
	assert(block.row < Screen::MAX_ROW);
	assert(block.col < Screen::MAX_COL);

	return map[block.row][block.col].type == Cell::Type::UNCLAIMED
					|| map[block.row][block.col].type == Cell::Type::CLAIMED;
}



void Hedgehog::spawn_enemy() {
	enemies.push_back(spawner);
	score += 2;
}



void Hedgehog::move_enemies() {
	for (Point<size_t> &e : enemies) {
		size_t min_pheromones = map[e.row][e.col].pheromones - 1;
		bool open_up = false;
		size_t pheromones_up = 0;
		if (e.row && map[e.row-1][e.col].type == Cell::Type::EMPTY) {
			open_up = true;
			pheromones_up = map[e.row-1][e.col].pheromones;
			min_pheromones = min(min_pheromones, pheromones_up);
		}
		bool open_left = false;
		size_t pheromones_left = 0;
		if (e.col && map[e.row][e.col-1].type == Cell::Type::EMPTY) {
			open_left = true;
			pheromones_left = map[e.row][e.col-1].pheromones;
			min_pheromones = min(min_pheromones, pheromones_left);
		}
		bool open_down = false;
		size_t pheromones_down = 0;
		if (e.row < Screen::MAX_ROW-1 && map[e.row+1][e.col].type
						== Cell::Type::EMPTY) {
			open_down = true;
			pheromones_down = map[e.row+1][e.col].pheromones;
			min_pheromones = min(min_pheromones, pheromones_down);
		}
		bool open_right = false;
		size_t pheromones_right = 0;
		if (e.col < Screen::MAX_COL-1 && map[e.row][e.col+1].type
						== Cell::Type::EMPTY) {
			open_right = true;
			pheromones_right = map[e.row][e.col+1].pheromones;
			min_pheromones = min(min_pheromones, pheromones_right);
		}

		vector<Direction> good_dirs;
		if (open_up && pheromones_up == min_pheromones) {
			good_dirs.push_back(Direction::UP);
		}
		if (open_left && pheromones_left == min_pheromones) {
			good_dirs.push_back(Direction::LEFT);
		}
		if (open_down && pheromones_down == min_pheromones) {
			good_dirs.push_back(Direction::DOWN);
		}
		if (open_right && pheromones_right == min_pheromones) {
			good_dirs.push_back(Direction::RIGHT);
		}

		Direction dir = Direction::NONE;
		if (!good_dirs.empty()) {
			dir = good_dirs[ran.get_size_t(0, good_dirs.size())];
		}

		switch (dir) {
			case Direction::UP:
				--e.row;
				break;
			case Direction::LEFT:
				--e.col;
				break;
			case Direction::DOWN:
				++e.row;
				break;
			case Direction::RIGHT:
				++e.col;
				break;
			case Direction::NONE:
				{}
		}

		++map[e.row][e.col].pheromones;
	}
}



bool Hedgehog::collision() const {
	for (Point<size_t> e : enemies) {
		if (hero.row == e.row && hero.col == e.col) return true;
	}
	return false;
}