#include "ricochet.h"
#include <cassert>

using namespace std;

Ricochet::Ricochet(Screen &screen_in)
: Speed_game{screen_in} { }

Ricochet::~Ricochet() { }

const size_t Ricochet::START_WIDTH = 6;
const char Ricochet::PLATFORM = '_';
const char Ricochet::BALL = 'x';


void Ricochet::prepare() {
    screen.clear();

    pos_first.row = Screen::MAX_ROW - 5;
    pos_first.col = (Screen::MAX_COL / 2) - 2;

    for (size_t i = 0; i < Screen::MAX_COL; ++i) {
        screen(Screen::MAX_ROW - 2, i) = '-';
    }

    width = START_WIDTH;

    pos_x.row = pos_first.row - 3;
    pos_x.col = Screen::MAX_COL/2;

    screen(pos_x.row, pos_x.col) = BALL;

    for (size_t i = pos_first.col; i < width + pos_first.col; ++i) {
        screen(pos_first.row, i) = PLATFORM;
    }


    right = true;
    up = true;

    score = 0;
}

Ricochet::Interval Ricochet::get_interval(size_t difficulty) {
    double framerate = pow(1.25, static_cast<double>(difficulty)) * 9;
	return Interval(1000 / framerate);
}

void Ricochet::handle_keypress(Screen::Key key) {
    switch (key) {
		case Screen::Key::UP:
			break;
		case Screen::Key::LEFT:
            if (pos_first.col != 0) {
                --pos_first.col;
                screen(pos_first.row, pos_first.col + width) = ' ';
                screen(pos_first.row, pos_first.col) = PLATFORM;
                break;
            }
			break;
		case Screen::Key::DOWN:
			break;
		case Screen::Key::RIGHT:
            if ((pos_first.col + width) != Screen::MAX_COL) {
                screen(pos_first.row, pos_first.col) = ' ';
                screen(pos_first.row, pos_first.col + width) = PLATFORM;
                ++pos_first.col;
                break;
            }
			break;
		case Screen::Key::SPACE:
            break;
        case Screen::Key::OTHER:
            {}
	}
}

bool Ricochet::tick() {

    dir();
    
    if (up && right) {
        screen(pos_x.row, pos_x.col) = ' ';
        --pos_x.row;
        ++pos_x.col;
        screen(pos_x.row, pos_x.col) = BALL;
    }

    else if (!up && right) {
        screen(pos_x.row, pos_x.col) = ' ';
        ++pos_x.row;
        ++pos_x.col;
        screen(pos_x.row, pos_x.col) = BALL;
    }

    else if (up && !right) {
        screen(pos_x.row, pos_x.col) = ' ';
        --pos_x.row;
        --pos_x.col;
        screen(pos_x.row, pos_x.col) = BALL;
    }

    else {
        screen(pos_x.row, pos_x.col) = ' ';
        ++pos_x.row;
        --pos_x.col;
        screen(pos_x.row, pos_x.col) = BALL;
    }

    
    return false;


}

void Ricochet::dir() {
    if (pos_x.col == 0 || pos_x.col == Screen::MAX_COL - 1 || pos_x.row == 0 || pos_x.row == Screen::MAX_ROW - 3) {
        if (pos_x.col == 0) {
        right = true;
        }

        else if (pos_x.col == Screen::MAX_COL - 1) {
            right = false;
        }

        else if (pos_x.row == 0) {
            up = false;
        }

        else {
            up = true;
        }
    }
}

//TODO: he dies when hits corner (upper right, more????)