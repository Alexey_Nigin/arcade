#ifndef SELECTOR_H
#define SELECTOR_H


// Usage:
//     Selector selector(screen);
//     vector<string> options = {"option", "option", "", "cool option"};
//     size_t choice = selector.show(options, 5);





#include "screen.h"



class Selector {
public:

	// M: this
	// E: Creates a selector with a given screen
	Selector(Screen &screen_in);

	// R: start_row + options.size() <= Screen::MAX_ROW
	//    5 + options[i].size() <= Screen::MAX_COL
	//    options contains a non-empty string
	// M: this, screen, keyboard
	// E: Shows a selector with options and lets the user choice any option that
	//    isn't an empty string. Returns the index of the chosen option. The
	//    0th option appears at start_row. Doesn't erase existing content on
	//    parts of the screen
	size_t show(const std::vector<std::string> &options, size_t start_row);

private:

	Screen &screen;
	
	const std::vector<std::string> *options_ptr;
	size_t start_row_copy, curr_option, old_option;

	// R: options_ptr and start_row_copy are initialised
	// E: Checks REQUIRES clause for show(), returns true iff invariants hold
	bool check_invariants() const;

	// R: check_invariants()
	// M: screen
	// E: Prints option labels onto the screen
	void print_labels();

	// R: check_invariants()
	// M: this->curr_option, this->old_option, screen
	// E: Points selector to the first non-null option and prints an arrow
	void select_first();

	// R: check_invariants() && curr_option, old_option are initialised
	// M: this->curr_option, this->old_option, screen, keyboard
	// E: Lets the user change and select an option, final selected option is
	//    stored in curr_option
	void ask_user();

	// R: check_invariants() && curr_option, old_option are initialised
	// M: this->curr_option, this->old_option, screen
	// E: Selects the item above previously selected, does nothing if there is
	//    no such item
	void move_up();

	// R: check_invariants() && curr_option, old_option are initialised
	// M: this->curr_option, this->old_option, screen
	// E: Selects the item below previously selected, does nothing if there is
	//    no such item
	void move_down();

	// R: check_invariants() && curr_option, old_option are initialised
	// M: this->old_option, screen
	// E: Draws the arrow in the right position and updates the screen
	void draw_arrow();

	Selector() = delete;

};

#endif