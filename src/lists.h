#ifndef LISTS_H
#define LISTS_H

#include <string>
#include <vector>





const std::vector<std::string> GAME_LIST = {
	"block_blast",
	"hedgehog",
	"snake",
	"space_trip"
};



const std::vector<std::string> DIFFICULTY_LIST = {
	"0",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9"
};

#endif