#include "block_blast.h"
#include <stack>
#include <cmath>

using namespace std;





Block_blast::Block_blast(Screen &screen_in)
: Speed_game{screen_in} { }



Block_blast::~Block_blast() { }



const string Block_blast::BLOCKS = "#Xo:";
const size_t Block_blast::START_HEIGHT = 4;
const char Block_blast::SHOOTER = 'U';



void Block_blast::prepare() {
	screen.clear();

	for (size_t i = 0; i < Screen::MAX_COL; ++++i) {
		for (size_t j = 0; j < START_HEIGHT; ++++j) {
			screen(j, i) = get_ran_char();
			screen(j, i + 1) = screen(j, i);
			screen(j + 1, i) = screen(j, i);
			screen(j + 1, i + 1) = screen(j, i);
		}
	}

	shoot_next = false;

	pos_s.row = Screen::MAX_ROW - 3;
	pos_s.col = Screen::MAX_COL / 2;
	pos_c.row = pos_s.row - 1;
	pos_c.col = pos_s.col;

	for (size_t i = 0; i < Screen::MAX_COL; ++i) {
		screen(Screen::MAX_ROW - 2, i) = '-';
	}

	count = 1;
	count_tick = 0;

	screen(pos_s.row, pos_s.col) = SHOOTER;
	put_char();
	score = 0;
}



Block_blast::Interval Block_blast::get_interval(size_t difficulty) {
	double framerate = pow(1.25, static_cast<double>(difficulty)) * 9;
	return Interval(1000 / framerate);
}



void Block_blast::handle_keypress(Screen::Key key) {
	switch (key) {
		case Screen::Key::UP:
			break;
		case Screen::Key::LEFT:
			screen(pos_s.row, pos_s.col) = ' ';
			screen(pos_c.row, pos_c.col) = ' ';
			pos_s.col = left(pos_s.col);
			if (screen(pos_c.row, left(pos_c.col)) == ' ') {
				pos_c.col = left(pos_c.col);
			}
			screen(pos_s.row, pos_s.col) = SHOOTER;
			screen(pos_c.row, pos_c.col) = c;
			break;
		case Screen::Key::DOWN:
			break;
		case Screen::Key::RIGHT:
			screen(pos_s.row, pos_s.col) = ' ';
			screen(pos_c.row, pos_c.col) = ' ';
			pos_s.col = right(pos_s.col);
			if (screen(pos_c.row, right(pos_c.col)) == ' ') {
				pos_c.col = right(pos_c.col);
			}
			screen(pos_s.row, pos_s.col) = SHOOTER;
			screen(pos_c.row, pos_c.col) = c;
			break;
		case Screen::Key::SPACE:
			shoot_next = true;
		case Screen::Key::OTHER:
			{}
	}
}

bool Block_blast::tick() {
	screen(pos_c.row, pos_c.col) = ' ';

	bool is_empty = true;
	for(size_t i = 0; i < Screen::MAX_ROW - 5; ++i) {
		for (size_t j = 0; j < Screen::MAX_COL; ++j) {
			if (screen(i, j) != ' ') {
				is_empty = false;
			}
		}
	}
	if (is_empty) score += 2;

	for (size_t k = 0; k < Screen::MAX_COL; ++k) {
		if (screen(Screen::MAX_ROW - 5, k) != ' ') {
			return true;
		} 
	}

	screen(pos_c.row, pos_c.col) = c;
	
	if (shoot_next) {
		shoot();
	}

	++count_tick;
	if (static_cast<double>(count_tick) > 2000/sqrt(count)) {
		screen(pos_c.row, pos_c.col) = ' ';
		for (size_t k = 0; k < Screen::MAX_COL; ++k) {
			if (screen(Screen::MAX_ROW - 6, k) != ' ') {
				return true;
			} 
			if (screen(Screen::MAX_ROW - 5, k) != ' ') {
				return true;
			} 
		}
		for(size_t i = Screen::MAX_ROW - 5; i--;) {
			for (size_t j = 0; j < Screen::MAX_COL; ++j) {
				screen(i + 2, j) = (screen(i, j));
				is_empty = false;
			}
		}
		for (size_t i = 0; i < Screen::MAX_COL; ++++i) {
			for (size_t j = 0; j < 2; ++++j) {
				screen(j, i) = get_ran_char();
				screen(j, i + 1) = screen(j, i);
				screen(j + 1, i) = screen(j, i);
				screen(j + 1, i + 1) = screen(j, i);
			}
		}
		count_tick = 0;
		++count;   
		screen(pos_c.row, pos_c.col) = c;
	}

	print_bar();
	return false;
}



char Block_blast::get_ran_char() {
	return BLOCKS[ran.get_size_t(0, BLOCKS.length())];
}



void Block_blast::put_char() {
	c = get_ran_char();
	pos_c.row = pos_s.row - 1;
	pos_c.col = pos_s.col;
	screen(pos_c.row, pos_c.col) = c;
	shoot_next = false;
}



void Block_blast::shoot() {
	if (!pos_c.row) {
		screen(pos_c.row, pos_c.col) = ' ';
		put_char();
	} else if (screen(pos_c.row - 1, pos_c.col) == ' ') {
		screen(pos_c.row, pos_c.col) = ' ';
		--pos_c.row;
		screen(pos_c.row, pos_c.col) = c;
	} else {
		bool hit_up = (screen(pos_c.row - 1, pos_c.col) == c);
		bool hit_left = (screen(pos_c.row, left(pos_c.col)) == c);
		bool hit_down = (screen(pos_c.row + 1, pos_c.col) == c);
		bool hit_right = (screen(pos_c.row, right(pos_c.col)) == c);
		if (hit_up || hit_left || hit_down || hit_right) chain(pos_c);
		put_char();
	}

}

void Block_blast::chain(Point<size_t> hit) {
	stack<Point<size_t>> waitlist;
	char victim = screen(hit.row, hit.col);
	++score;
	waitlist.push(hit);
	while (!waitlist.empty()) {
		Point<size_t> boom = waitlist.top();
		waitlist.pop();
		screen(boom.row, boom.col) = ' ';
		++score;
		if (boom.row > 0 && screen(boom.row - 1, boom.col) == victim) {
			waitlist.emplace(boom.row - 1, boom.col);
		}
		if (screen(boom.row, left(boom.col)) == victim) {
			waitlist.emplace(boom.row, left(boom.col));
		}
		if (boom.row < Screen::MAX_ROW - 1
						&& screen(boom.row + 1, boom.col) == victim) {
			waitlist.emplace(boom.row + 1, boom.col);
		}
		if (screen(boom.row, right(boom.col)) == victim) {
			waitlist.emplace(boom.row, right(boom.col));
		}
	}
}

void Block_blast::print_bar() {
	for (size_t i = 0; i < Screen::MAX_COL; ++i) {
		screen(Screen::MAX_ROW - 1, i) = ' ';
	}
	screen.print(Screen::MAX_ROW - 1, 1, "score: "
					+ to_string(score * multiplier));
	int num = static_cast<int>(2000/sqrt(count)
					- static_cast<double>(count_tick));
	string fall_text = "time until drop: " + to_string(num);
	screen.print(Screen::MAX_ROW - 1, Screen::MAX_COL - fall_text.length(),
					fall_text);
}



size_t Block_blast::left(size_t col) const {
	return col ? (col - 1) : (Screen::MAX_COL - 1);
}



size_t Block_blast::right(size_t col) const {
	return col < (Screen::MAX_COL - 1) ? (col + 1) : 0;
}