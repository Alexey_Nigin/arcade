#include "menu.h"
#include "selector.h"
#include "setup.h"
#include "quit.h"
#include <string>
#include <vector>
#include <cassert>

using namespace std;





Menu::Menu(Screen &screen_in, Scores &scores_in)
: screen{screen_in}, scores{scores_in}, game_index{0},
				difficulty_index{0}, current_score{Scores::NO_SCORE},
				game_change{true}, difficulty_change{true}, want_menu{false} {}



void Menu::run() {
	Setup setup(screen, scores);
	while(true) {
		if (game_change) get_game();
		if (difficulty_change) get_difficulty();
		if (want_menu) continue;
		current_score = setup.play(game_index, difficulty_index);
		show_scores();
	}
}

void Menu::get_game() {
	want_menu = false;
	screen.clear();
	screen.print(1, 1, "choose game:");
	Selector game_select(screen);
	vector<string> game_options = GAME_LIST;
	game_options.push_back("");
	game_options.push_back("quit");
	game_index = game_select.show(game_options, 5);
	if (game_index == GAME_LIST.size() + 1) quit();
}

void Menu::get_difficulty() {
	screen.clear();
	screen.print(1, 1, "choose difficulty for " + GAME_LIST[game_index] + ':');
	Selector difficulty_select(screen);
	vector<string> difficulty_options = DIFFICULTY_LIST;
	difficulty_options.push_back("");
	difficulty_options.push_back("change game");
	difficulty_options.push_back("quit");
	difficulty_index = difficulty_select.show(difficulty_options, 5);
	if (difficulty_index == DIFFICULTY_LIST.size() + 1) want_menu = true;
	if (difficulty_index == DIFFICULTY_LIST.size() + 2) quit();
}

void Menu::show_scores() {
	screen.clear();
	vector<int> scores_print = scores.get(game_index, difficulty_index);
	screen.print(1, 1, "your score: ");
	screen.print(1, 13, to_string(current_score));
	screen.print(3, 1, "high scores:");
	for (size_t i = 0; i < scores_print.size(); ++i) {
		screen.print(5 + i, 1, to_string(i));
		screen.print(5 + i, 2, "] ");
		if (scores_print[i] != Scores::NO_SCORE) {
			screen.print(5 + i, 4, to_string(scores_print[i]));
		}
		else {
			screen.print(5 + i, 4, "---");
		}
		
	}
	Selector score_select(screen);
	vector<string> score_opt = {"play again", "change difficulty", "change game", "quit"};
	size_t option_index = score_select.show(score_opt, 16);
	if (option_index == 0) {
		difficulty_change = false;
		game_change = false;
		return;
	}
	if (option_index == 1) {
		difficulty_change = true;
		game_change = false;
		return;
	}
	if (option_index == 2) {
		difficulty_change = true;
		game_change = true;
	}
	else {
		quit();
	}
	
}