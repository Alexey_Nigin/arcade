#ifndef RANDOMIZER_H
#define RANDOMIZER_H

// Usage:
//     Randomizer randomizer;
//     size_t r = randomizer.get_size_t(low, high);
//
// Creating randomizers is expensive, so don't do it too often
// All ranges are stl-style: [low, high)





#include <cstddef>
#include <random>



class Randomizer {
public:

	// M: this
	// E: Creates a randomizer with a random seed
	// N: Expensive!
	Randomizer();

	// M: this
	// E: Returns a size_t in the range [low, high)
	size_t get_size_t(size_t low, size_t high);

private:

	std::mt19937 engine;

};

#endif