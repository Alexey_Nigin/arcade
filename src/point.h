#ifndef POINT_H
#define POINT_H





template <class T>
class Point {
public:

		// Variables

		T row, col;

		// Functions

		// M: this
		// E: Creates a point at (0, 0)
		Point();

		// M: this
		// E: Creates a point at (row_in, col_in)
		Point(T row_in, T col_in);

		// M: this
		// E: Moves the point to (row_in, col_in)
		void set(T row_in, T col_in);

};



template <class T>
Point<T>::Point()
: row{0}, col{0} {}



template <class T>
Point<T>::Point(T row_in, T col_in)
: row{row_in}, col{col_in} {}



template <class T>
void Point<T>::set(T row_in, T col_in) {
	row = row_in;
	col = col_in;
}

#endif