#include "speed_game.h"
#include <thread>

using namespace std;





Speed_game::Speed_game(Screen &screen_in)
: Game{screen_in} {}



int Speed_game::play(size_t difficulty) {
    multiplier = static_cast<int>(difficulty + 1);
    prepare();
    Interval diff = get_interval(difficulty);
    bool game_over = false;

    while (!game_over) {
        Screen::Key key = Screen::Key::OTHER;
        while (screen.get_key(key, false)) {
            handle_keypress(key);
        }
        game_over = tick();
        screen.update();
        this_thread::sleep_for(diff);
    }
    return score * multiplier;
}



Speed_game::~Speed_game() {}