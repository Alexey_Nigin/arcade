#ifndef RICOCHET_H
#define RICOCHET_H

#include "point.h"
#include "speed_game.h"
#include "randomizer.h"
#include <string>

class Ricochet : public Speed_game {
public:

    // M: this
    // E: Creates a ricochet attached to given screen
    Ricochet(Screen &screen_in);


    // M: this
    // E: Destroys this
    virtual ~Ricochet();

private:
  

    //Variables

    static const size_t START_WIDTH;
    static const char PLATFORM;
    static const char BALL;
    bool right;
    bool up;
    
    Randomizer ran;
    Point<size_t> pos_first, pos_x;

    size_t width;

    // Functions

    // M: this
	// E: Prepares the game by initialising all relevant member variables
	virtual void prepare() override;

	// R: difficulty is valid
	// E: Returns the interval between screen updates for given difficulty
	virtual Interval get_interval(size_t difficulty) override;

	// M: this
	// E: Handles a keypress by changing appropriate member variables
	virtual void handle_keypress(Screen::Key key) override;

	// M: this
	// E: Calculates a single tick of the game and returns true iff game over
	virtual bool tick() override;

    void dir();

    bool platform();


};









#endif