#ifndef SETUP_H
#define SETUP_H


// Usage:
//     Setup setup(screen, keyboard, scores);
//     bool success == setup.play(game_index, difficulty_index);





#include "screen.h"
#include "score.h"


class Setup {
public:

	// M: this
	// E: Creates a setup attached to given screen and scores
	Setup(Screen &screen_in, Scores &scores_in);

	// M: screen, scores
	// E: Plays game_index at difficulty_index and returns score of game
	int play(size_t game_index, size_t difficulty_index) const;

private:

	Screen &screen;
	Scores &scores;

	Setup() = delete;
};

#endif