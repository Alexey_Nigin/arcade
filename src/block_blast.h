#ifndef BLOCK_BLAST_H
#define BLOCK_BLAST_H

#include "speed_game.h"
#include "point.h"
#include "randomizer.h"
#include <string>

class Block_blast : public Speed_game {
public:

	// M: this
	// E: Creates a block_blast attached to given screen and keyboard
	Block_blast(Screen &screen_in);

	// M: this
	// E: Destroys this
	virtual ~Block_blast();

private:

	// Types

	enum class Direction {LEFT, RIGHT};    

	//Variables

	static const std::string BLOCKS;
	static const size_t START_HEIGHT;
	static const char SHOOTER;
	
	Randomizer ran;
	Point<size_t> pos_s, pos_c;
	size_t count;
	size_t count_tick;
	char c;
	bool shoot_next;

	// Functions

	// M: this
	// E: Prepares the game by initialising all relevant member variables
	virtual void prepare() override;

	// R: difficulty is valid
	// E: Returns the interval between screen updates for given difficulty
	virtual Interval get_interval(size_t difficulty) override;

	// M: this
	// E: Handles a keypress by changing appropriate member variables
	virtual void handle_keypress(Screen::Key key) override;

	// M: this
	// E: Calculates a single tick of the game and returns true iff game over
	virtual bool tick() override;

	// E: Returns a random block character
	// N: Not const because it uses ran
	char get_ran_char();

	// M: screen, this->{pos_c, shoot_next}
	// E: Places a new bullet on top of the cannon
	void put_char();

	// M: screen, this->{pos_c, shoot_next}
	// E: Moves the bullet one row up, handling collisions
	void shoot();

	// M: screen
	// E: Propagates explosion from hit
	void chain(Point<size_t> hit);

	// M: screen
	// E: Updates the bar at the bottom of the screen
	void print_bar();

	// R: col < Screen::MAX_COL
	// E: Returns number of column to the left of col, wrapping around
	size_t left(size_t col) const;

	// R: col < Screen::MAX_COL
	// E: Returns number of column to the right of col, wrapping around
	size_t right(size_t col) const;

};

#endif