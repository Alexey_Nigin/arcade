#include "space_trip.h"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <cassert>

using namespace std;





Space_trip::Space_trip(Screen &screen_in)
: Speed_game{screen_in} {}



Space_trip::~Space_trip() {}



Space_trip::Bullet::Bullet()
: pos{}, boom{false} {}



Space_trip::Bullet::Bullet(size_t row_in, size_t col_in, bool boom_in)
: pos{row_in, col_in}, boom{boom_in} {}



const string Space_trip::MODULE_FILE = "space_trip.dat";
const char Space_trip::DELIM = '|';
const char Space_trip::PLAYER = '>';
const char Space_trip::WEAPON = '~';
const char Space_trip::BOOM = '@';
const size_t Space_trip::BAR_ROWS = 3;
const size_t Space_trip::CHARGE_TIME = 80;
const size_t Space_trip::LASER_COST = 5;
const size_t Space_trip::MOD_DISTANCE = 16;
const size_t Space_trip::CURVE_START = 8;
const size_t Space_trip::CURVE = 2;



void Space_trip::prepare() {
	screen.clear();

	// Bar
	string dashes;
	dashes.resize(Screen::MAX_COL, '-');
	screen.print(BAR_ROWS - 1, 0, dashes);
	screen.print(0, 1, "power");
	screen.print(0, Screen::MAX_COL - 9, "coolness");

	// Player
	player_pos = (Screen::MAX_ROW - BAR_ROWS) / 2;
	queue<Direction>{}.swap(want_move);

	// Station
	station.clear();
	string spaces;
	spaces.resize(Screen::MAX_COL, ' ');
	station.resize(Screen::MAX_ROW - BAR_ROWS, spaces);
	load_modules();
	mods_placed = 0;

	// Weapons
	bullets.clear();
	want_bullets = 0;
	want_laser = false;

	power = 0;
	time = 0;
	score = 0;
}



Space_trip::Interval Space_trip::get_interval(size_t difficulty) {
	double framerate = pow(1.5, static_cast<double>(difficulty)) * 2;
	return Interval(1000 / framerate);
}



void Space_trip::handle_keypress(Screen::Key key) {
	switch (key) {
		case Screen::Key::UP:
			want_move.push(Direction::UP);
			break;
		case Screen::Key::LEFT:
			++want_bullets;
			break;
		case Screen::Key::DOWN:
			want_move.push(Direction::DOWN);
			break;
		case Screen::Key::RIGHT:
			++want_bullets;
			break;
		case Screen::Key::SPACE:
			want_laser = true;
		case Screen::Key::OTHER:
			{}
	}
}



bool Space_trip::tick() {
	if (time % CHARGE_TIME == 0) ++power;

	move_station();
	for (size_t i = 0; i < station.size(); ++i) {
		screen.print(i + BAR_ROWS, 0, station[i].substr(0, Screen::MAX_COL));
	}

	if (!move_player()) {
		update_bar();
		return true;
	}

	boom_bullets();
	if (want_bullets) {
		if (power) {
			fire_bullet();
		}
		--want_bullets;
	}
	collide_bullets();
	move_bullets();
	collide_bullets();
	draw_bullets();

	if (want_laser) {
		if (power >= LASER_COST) {
			fire_laser();
		}
		want_laser = false;
	}

	++time;
	++score;
	update_bar();

	return false;
}



void Space_trip::load_modules() {
	modules.clear();

	// Open file
	ifstream file(MODULE_FILE);
	assert(file.is_open());

	for (size_t row = 0; row < Screen::MAX_ROW - BAR_ROWS; ++row) {
		// Read a line
		string raw_str;
		getline(file, raw_str);
		assert(file);

		// Resize modules
		size_t num_modules = count(raw_str.begin(), raw_str.end(), DELIM);
		if (row == 0) {
			modules.resize(num_modules,
							vector<string>{Screen::MAX_ROW - BAR_ROWS});
		} else {
			assert(num_modules == modules.size());
		}

		for (size_t mod = 0; mod < num_modules; ++mod) {
			// Get first module from raw_str and erase it from there
			size_t pos = raw_str.find(DELIM);
			string str = raw_str.substr(0, pos);
			raw_str.erase(0, pos + 1);

			// Put this module part where it belongs
			modules[mod][row] = str;
		}
	}

	assert(check_modules());
}



bool Space_trip::check_modules() const {
	for (const auto mod : modules) {
		size_t bigness = mod[0].size();
		for (size_t row = 1; row < Screen::MAX_ROW - BAR_ROWS; ++row) {
			if (mod[row].size() != bigness) return false;
		}
	}
	return true;
}



void Space_trip::move_station() {
	for (string &row : station) {
		row.erase(0, 1);
	}

	if (station[0].size() < Screen::MAX_COL) {
		// Add a new module

		// Which one?
		size_t module = ran.get_size_t(mods_placed ? 0 : CURVE,
						mods_placed + CURVE_START * CURVE) / CURVE;
		module = min(module, modules.size() - 1);

		// Place it!
		string separator;
		separator.resize(MOD_DISTANCE, ' ');
		for (size_t row = 0; row < Screen::MAX_ROW - BAR_ROWS; ++row) {
			station[row] += modules[module][row] + separator;
		}

		++mods_placed;
	}
}



void Space_trip::boom_bullets() {
	auto p = [](const Bullet &b) {return b.boom;};
	bullets.erase(remove_if(bullets.begin(), bullets.end(), p), bullets.end());
}



void Space_trip::fire_bullet() {
	--power;
	bullets.emplace_back(player_pos, 1, false);
}



void Space_trip::collide_bullets() {
	for (Bullet &b : bullets) {
		if (station[b.pos.row][b.pos.col] != ' ') {
			station[b.pos.row][b.pos.col] = ' ';
			b.boom = true;
		}
	}
}



void Space_trip::move_bullets() {
	for (Bullet &b : bullets) {
		if (!b.boom) ++b.pos.col;
	}

	auto p = [](const Bullet &b) {return b.pos.col >= Screen::MAX_COL;};
	bullets.erase(remove_if(bullets.begin(), bullets.end(), p), bullets.end());
}



void Space_trip::draw_bullets() {
	for (auto &b : bullets) {
		screen(b.pos.row + BAR_ROWS, b.pos.col) = b.boom ? BOOM : WEAPON;
	}
}



void Space_trip::fire_laser() {
	assert(power >= LASER_COST);

	power -= LASER_COST;

	for (size_t col = 1; col < Screen::MAX_COL; ++col) {
		station[player_pos][col] = ' ';
		screen(player_pos + BAR_ROWS, col) = WEAPON;
	}
}



bool Space_trip::move_player() {
	while (!want_move.empty()) {
		if (want_move.front() == Direction::DOWN &&
						player_pos < Screen::MAX_ROW-BAR_ROWS-1) {
			++player_pos;
		} else if (want_move.front() == Direction::UP && player_pos) {
			--player_pos;
		}
		want_move.pop();
		if (!collide_player()) return false;
	}
	if (!collide_player()) return false;

	screen(player_pos + BAR_ROWS, 1) = PLAYER;
	return true;
}



bool Space_trip::collide_player() {
	if (station[player_pos][1] != ' ') {
		if (power < LASER_COST) {
			// Die
			power = 0;
			screen(player_pos + BAR_ROWS, 1) = BOOM;
			return false;
		} else {
			// Survive
			power -= LASER_COST;
			station[player_pos][1] = ' ';
		}
	}
	return true;	
}



void Space_trip::update_bar() {
	string spaces;
	spaces.resize(Screen::MAX_COL, ' ');
	screen.print(1, 0, spaces);
	string power_text = to_string(power);
	string cool_text = to_string(score*multiplier);
	screen.print(1, 1, power_text);
	screen.print(1, Screen::MAX_COL - cool_text.size() - 1, cool_text);
}